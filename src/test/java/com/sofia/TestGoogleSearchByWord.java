package com.sofia;

import com.sofia.drivers.DriverManager;
import com.sofia.element.CustomWebElement;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertTrue;

public class TestGoogleSearchByWord {
    private static final Logger LOG = LogManager.getLogger(TestGoogleSearchByWord.class);
    private static final String TEST_BASE_PAGE = "https://google.com";

    private WebDriver driver;

    @BeforeTest
    public void setUpDriver() {
        driver = DriverManager.getDriverInstance();
    }

    @Test
    public void searchWordResults() {
        driver.get(TEST_BASE_PAGE);
        CustomWebElement customElement = new CustomWebElement(driver.findElement(By.name("q")), driver);
        customElement.sendKeys("Apple");
        customElement.submit();

        LOG.info("Title: " + driver.getTitle());
        Assert.assertTrue(driver.getTitle().toLowerCase().contains("apple"));
        driver.findElement(By.cssSelector("a.q.qs")).click();
        //WebElement imagePageElement = driver.findElement(By.cssSelector("div.hdtb-mitem.hdtb-msel.hdtb-imb"));

        CustomWebElement imagePageElement = new CustomWebElement(driver.findElement(By.xpath("//*[@aria-label='Додатки Google']")), driver);
        assertTrue(imagePageElement.getAttribute("aria-expanded"), true);
    }

    @Test
    public void testHabrLogin() {
        driver.get("https://habr.com/ru/post/152971/");
        CustomWebElement button = new CustomWebElement(driver.findElement(By.id("login")), driver);
        button.click();
        CustomWebElement field = new CustomWebElement(driver.findElement(By.name("password")), driver);
        assertTrue(field.getAttribute("data-required"), true);
    }

    @AfterTest
    public void endTest() {
        driver.quit();
    }
}
