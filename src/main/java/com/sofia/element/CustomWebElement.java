package com.sofia.element;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.Optional;

public class CustomWebElement implements WebElement {
    private final static Logger LOGGER = LogManager.getLogger(CustomWebElement.class);
    private final WebElement element;
    private final WebDriver driver;

    public CustomWebElement(WebElement element, WebDriver driver) {
        this.element = element;
        this.driver = driver;
    }

    @Override
    public boolean isDisplayed() {
        boolean isDisplayed = false;
        try {
            isDisplayed = element.isDisplayed();
        } catch (NoSuchElementException e) {
            LOGGER.error("There is no such element" + element);
            LOGGER.error(e);
        }
        return isDisplayed;
    }

    @Override
    public void click() {
        try {
            waitUntilPageIsLoaded();
            element.click();
            LOGGER.info("Clicked the element " + element);
        } catch (StaleElementReferenceException e) {
            LOGGER.error("Seems element is dynamic or old " + element);
            LOGGER.error(e);
            jsClick();
        } catch (NoSuchElementException e) {
            LOGGER.error("There is no such element" + element);
            LOGGER.error(e);
        } catch (ElementClickInterceptedException e) {
            LOGGER.error("Click was intercepted");
            jsClick();
        }
    }

    public void waitUntilPageIsLoaded() {
        new WebDriverWait(driver, 10).until(webDriver ->
                ((JavascriptExecutor) webDriver).executeScript("return document.readyState").equals("complete"));
    }

    public void jsClick() {
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", element);
        LOGGER.info("Clicked with JS on element " + element);
    }

    @Override
    public void submit() {
        LOGGER.info("Submit " + element);
        element.submit();
    }

    @Override
    public void sendKeys(CharSequence... charSequences) {
        try {
            element.sendKeys(charSequences);
            LOGGER.info("Sending keys (text) to " + element);
        } catch (StaleElementReferenceException e) {
            LOGGER.error("Seems element is dynamic or old " + element);
            LOGGER.error(e);
        } catch (NoSuchElementException e) {
            LOGGER.error("There is no such element" + element);
            LOGGER.error(e);
        }
    }

    @Override
    public void clear() {
        LOGGER.info("Clear value of " + element);
        element.clear();
    }

    @Override
    public String getTagName() {
        LOGGER.info("Getting TagName of element " + element);
        return element.getTagName();
    }

    @Override
    public String getAttribute(String s) {
        String attribute = "";
        try {
            attribute = element.getAttribute(s);
            LOGGER.info("Got attribute " + attribute + " of " + element);
        } catch (NoSuchElementException e) {
            LOGGER.error("There is no such element" + element);
            LOGGER.error(e);
        }
        return attribute;
    }

    @Override
    public boolean isSelected() {
        boolean isSelected = false;
        try {
            isSelected = element.isSelected();
        } catch (NoSuchElementException e) {
            LOGGER.error("There is no such element" + element);
            LOGGER.error(e);
        }
        return isSelected;
    }

    @Override
    public boolean isEnabled() {
        boolean isEnabled = false;
        try {
            isEnabled = element.isEnabled();
        } catch (NoSuchElementException e) {
            LOGGER.error("There is no such element" + element);
            LOGGER.error(e);
        }
        return isEnabled;
    }

    @Override
    public String getText() {
        String text = "";
        try {
            LOGGER.info("Getting text of element");
            text = element.getText();
        } catch (NoSuchElementException e) {
            LOGGER.error("There is no such element to get text from" + element);
            LOGGER.error(e.getMessage());
        }
        return text;
    }

    @Override
    public List<WebElement> findElements(By by) {
        LOGGER.info("Find all element with by: " + by);
        return driver.findElements(by);
    }

    @Override
    public WebElement findElement(By by) {
        Optional<Object> foundElement = Optional.empty();
        try {
            LOGGER.info("Find element with by: " + by);
            foundElement = Optional.ofNullable(driver.findElement(by));
        } catch (NoSuchElementException e) {
            LOGGER.error("There is no such element to get text from" + element);
            LOGGER.error(e);
        }
        return (WebElement) foundElement.get();
    }

    @Override
    public Point getLocation() {
        LOGGER.info("Getting location of " + element);
        return element.getLocation();
    }

    @Override
    public Dimension getSize() {
        LOGGER.info("Getting size of " + element);
        return element.getSize();
    }

    @Override
    public Rectangle getRect() {
        LOGGER.info("Getting rect of " + element);
        return element.getRect();
    }

    @Override
    public String getCssValue(String s) {
        LOGGER.info("Getting css value of " + element);
        return element.getCssValue(s);
    }

    @Override
    public <X> X getScreenshotAs(OutputType<X> outputType) throws WebDriverException {
        return element.getScreenshotAs(outputType);
    }
}
