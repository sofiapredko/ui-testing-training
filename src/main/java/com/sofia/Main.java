package com.sofia;

import com.sofia.drivers.ThreadLocalDriver;
import com.sofia.drivers.ThreadSafeSingleton;
import org.openqa.selenium.WebDriver;

public class Main extends Thread {
    private static String getThreadName() {
        return "[" + Thread.currentThread().getName() + "] - ";
    }

    public static void main(String[] args) {
        Thread t1 = new Thread(() -> {
            WebDriver threadSafe = ThreadSafeSingleton.getDriverInstance();
            System.out.println(getThreadName() + threadSafe + "multi 1");
            WebDriver threadSafe12 = ThreadSafeSingleton.getDriverInstance();
            System.out.println(getThreadName() + threadSafe12 + "multi 1.2");
            WebDriver treadLocal = ThreadLocalDriver.getDriverInstance();
            System.out.println(getThreadName() + treadLocal + "treadlocal 2");
            threadSafe.quit();
            threadSafe12.quit();
            treadLocal.quit();
        });
        t1.start();

        Thread t2 = new Thread(() -> {
            WebDriver threadSafe = ThreadSafeSingleton .getDriverInstance();
            WebDriver treadLocal = ThreadLocalDriver.getDriverInstance();
            System.out.println(getThreadName() + threadSafe + "multi 2");
            System.out.println(getThreadName() + treadLocal + "treadlocal 2");
            threadSafe.quit();
            treadLocal.quit();
        });
        t2.start();
    }
}
