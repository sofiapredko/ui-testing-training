package com.sofia.pages.youtubepages;

import com.sofia.pages.BasePage;
import org.openqa.selenium.WebDriver;

public class HistoryPage extends BasePage {
    private static final String YOUTUBE_HISTORY_LINK = "/feed/history";

    public void navigateToHistoryPage() {
        driver.get(YOUTUBE_HOME_LINK + YOUTUBE_HISTORY_LINK);
    }
}
