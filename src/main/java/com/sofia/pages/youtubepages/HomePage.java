package com.sofia.pages.youtubepages;

import com.sofia.pages.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends BasePage {

    @FindBy(xpath = "//input[@id='search']")
    private WebElement searchField;

    @FindBy(id = "search-icon-legacy")
    private WebElement startSearchButton;

    @FindBy(id = "logo")
    private WebElement mainLogoToHomePage;

    public void clickOnSearchInput() {
        searchField.click();
    }

    public void typeInSearchField(String text) {
        searchField.sendKeys(text);
    }

    public void runSearchByText() {
        searchField.submit();
    }

    public void clickOnSearchButton() {
        startSearchButton.click();
    }

    public void navigateToHomePage() {
        driver.get(YOUTUBE_HOME_LINK);
    }

    public void returnToHomePage() {
        mainLogoToHomePage.click();
    }

}
