package com.sofia.pages;

import com.sofia.drivers.DriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import static com.sofia.drivers.DriverManager.getDriverInstance;

public class BasePage {
    protected static final String YOUTUBE_HOME_LINK = "https://www.youtube.com";
    protected WebDriver driver;
    private WebDriverWait webDriverWait;
    private static final int PAGE_LOAD_WAIT = 12;
    private static final int BUTTON_WAIT = 15;

    public BasePage() {
        driver = getDriverInstance();
        webDriverWait = new WebDriverWait(driver, 30);
        PageFactory.initElements(getDriverInstance(), this);
    }
}
